"""Модуль с обработчиками (вьюхами)."""
import os
import random

from flask import render_template, url_for, send_from_directory, redirect

from app import app


@app.route('/')
def index(number_of__bg_images=app.config['BG_IMAGES_N']):
    """Главная страница."""
    random.shuffle(app.config['BACKGROUND_IMAGES'])
    imgs = [url_for('static', filename='bg_images/' + x) for x
            in app.config['BACKGROUND_IMAGES'][:number_of__bg_images]]
    return render_template('index.html', title='', bg_imgs=imgs, my_color='red')


@app.route('/stuff_price')
def stuff():
    """Передает файл с ценами на товары."""
    for p in os.listdir('price-lists'):
        if p.startswith('Price_AquaDomus_Stuff'):
            path = p; break
    else:
        print('Файл "Price_AquaDomus_Stuff*" не найден в каталоге price-lists')
    return send_from_directory(directory='price-lists', path=path, as_attachment=True, max_age=0)


@app.route('/services_price')
def services():
    """Передает файл с ценами на обслуживание."""
    for p in os.listdir('price-lists'):
        if p.startswith('Price_AquaDomus_Services'):
            path = p; break
    else:
        print('Файл "Price_AquaDomus_Services*" не найден в каталоге price-lists')
    return send_from_directory(directory='price-lists', path=path, as_attachment=True, max_age=0)


@app.route('/contacts')
def contacts():
    """Страница с контактами."""
    return render_template('contacts.html', title='Контакты')


@app.route('/login')
def login():
    """Перенаправление на страницу залогинивания."""
    return redirect('/admin/login'), 301


@app.errorhandler(Exception)
def error(e):
    """Обработчик ошибок."""
    return render_template('error.html',
                           title=e.code,
                           text='Такой страницы не существует :-(' if e.code == 404 \
                               else e.description), e.code
