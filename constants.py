"""Модуль с константами. Смотри также 'templates/constants.html' 
и переменные задаваемые в начале 'static/style.css'.
"""

DEBUG = True        # Только для тестового сервера werkzeug
HOST = '0.0.0.0'    # Только для тестового сервера werkzeug
PORT = 5000         # Только для тестового сервера werkzeug

BG_IMAGES_N = 2  # Количество фотографий отображаемых на главной странице.
ENABLE_ADMINISTRATION = True  # Включать ли функции-обработчики путей администрирования.

MAX_CONTENT_LENGTH = 100 * 1024 * 1024  # 100Мб - максимальный суммарный размер загружаемых файлов.
PERMANENT_SESSION_LIFETIME = 30 * 60  # 30 мин - срок жизни session-cookie авторизации.

# import secrets; print(secrets.token_bytes(24))
SECRET_KEY = b"test_only_\x8d\xefz\x0c\x13\xf1h\x02r\tZ\xd0A\xca;lH\x99P\xde\x01\x9d\xe8("
PASSWORD_HASHING_ITERATIONS = 1000000  # Изменения вступят в силу только после смены пароля.

ENFORCE_HTTPS = True  # Для тестирования (и ТОЛЬКО для тестирования!) без HTTPS можно установить False.
SESSION_COOKIE_SECURE = True  # Для тестирования (и ТОЛЬКО для тестирования!) без HTTPS можно установить False.
SESSION_COOKIE_HTTPONLY = True
SESSION_COOKIE_SAMESITE = 'Lax'
