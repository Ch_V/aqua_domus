Run with `python main.py` (with default settings serves only on **https**  with self-signed certificate) or with Docker (serves on 80 (redirects http to https) and 443 posts).

You may login on `/login` with password `123`.
