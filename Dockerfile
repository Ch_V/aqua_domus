FROM python:slim-buster

ENV TZ EUROPE/Moscow
RUN useradd -m app
USER app
ENV PATH="/home/app/.local/bin:$PATH"

RUN pip install --no-cache-dir gunicorn
EXPOSE 5443 5080

RUN mkdir -p /home/app/aquadomus
WORKDIR /home/app/aquadomus
COPY --chown=app:app . .

RUN pip install --no-cache-dir -r requirements.txt

# for HTTPS traffic -p 443:5443 and for HTTP traffic -p 80:5080
CMD gunicorn --bind=:5443 \
        -w=5 \
        # --access-logfile - \
        # --error-logfile - \
        --certfile=TEST_ONLY_server.crt --keyfile=TEST_ONLY_server.key \
        app:app & \
    gunicorn --bind=:5080 \
        -w=1 \
        # --access-logfile - \
        # --error-logfile - \
        http_redirect:http_redirect & \
    wait
