"use strict";


function hide_iframe() {
    document.getElementById('iframe').contentWindow.location.replace("/gallery/0");
    document.getElementById("iframe").style.display = "none";
}


function show_iframe(img_n_path) {
    document.getElementById("iframe").style.display = "block";
    document.getElementById('iframe').contentWindow.location.replace(img_n_path);
    document.getElementById('iframe').focus();
}
