"use strict";


function hide_me() {
    /* Функция скрывания iframe-а. */
    parent.hide_iframe();
}


let img_n
function flip_iframe(direction) {
    /* Функция перелистывания фотографий.
     Параметр direction определяет направление перелистывания
     и может принимать следующие два значенияЖ '<-' и '->'.  */
    let url = document.getElementById('iframe-image').getAttribute('src');
    url = new URL('http://example.com/' + url);  // для дальнейшего получения get-параметра img_n.
    img_n = url.searchParams.get('img_n');
    switch (direction) {
        case '->':
            if (img_n < photos_len) {
                img_n = String(Number(img_n) + 1);
            }
            break;
        case '<-':
            if (img_n > 1) {
                img_n = String(Number(img_n) - 1);
            }
            break;
    }
    let res_url = document.location.pathname + "/inner?img_n=" + img_n;
    document.getElementById('iframe-image').setAttribute("src", res_url);
}


document.onkeydown = function(e) {
    /* Перелистывание клавишами клавиатуры. */
    switch(e.which) {
        case 27:
            parent.hide_iframe();
            break;

        case 37: // left
            flip_iframe('<-');
            break;

        case 38: // up
            flip_iframe('<-');
            break;

        case 39: // right
            flip_iframe('->');
            break;

        case 40: // down
            flip_iframe('->');
            break;

        default:
            parent.onkeydown(e);
            break;
    }
    e.preventDefault();
};


function onWheel(e) {
    /* Перелистывание колесиком мышки. */
    if (e.wheelDeltaX > 0 || e.wheelDeltaY > 0) flip_iframe('<-');
    if (e.wheelDeltaX < 0 || e.wheelDeltaY < 0) flip_iframe('->');
    e.preventDefault();
}
document.addEventListener("wheel", onWheel, { passive: false });


let touchPosX, touchPosY, flip;  // flip: boolean - немультитточ
document.ontouchstart = function(e){
    /* Запоминаем начальные позиции касания. */
    if (e.touches.length == 1) flip = 'true';
    else flip = 'false';
//     alert(flip)
    touchPosX = e.changedTouches[0].clientX;
    touchPosY = e.changedTouches[0].clientY;
}


document.ontouchend = function(e){
    /* Выполняем перелистывание при окончании касания. */
    const newTouchPosX = e.changedTouches[0].clientX;
    const newTouchPosY = e.changedTouches[0].clientY;
    const vw = Math.max(document.documentElement.clientWidth || 0, window.innerWidth || 0);
    // Проверем движение на более горизонтальность, чем вертикальность.
    if ((Math.abs(touchPosX - newTouchPosX) / Math.abs(touchPosY - newTouchPosY)) > 2 && flip){
        if(newTouchPosX + 0.1 * vw < touchPosX) {
            flip_iframe('->');
        }
        if(newTouchPosX > 0.1 * vw + touchPosX) {
            flip_iframe('<-');
        }
    }
}


function onTouchMove(e){
//     e.preventDefault();  // Для предотвращения сдвига базовой страницы (под iframe-ом при пролистывании фотографий пальцем)
                            // Но это не дает убрать полосу с адресом в мобильном браузере, так что пусть прокручивает.
    if (e.touches.length > 1) flip = false;  // Блокирует перелистывание если это мультитач.
}


document.addEventListener("touchmove", onTouchMove, { passive: false });
