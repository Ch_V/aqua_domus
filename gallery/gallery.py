"""Блюпринт с галерей с фотографиями."""

import os
import sqlite3
import sys
import urllib

from flask import Blueprint, render_template, g, abort, request, make_response

from common_components import slugify

gallery_bp = Blueprint(name='gallery', import_name=__name__, template_folder='templates', static_folder='static')


@gallery_bp.before_request
def connect_db():
    """Создаем подключение к базе данных галерей с информацией
    об адресе хранения файлов, url-слаге и порядковом коэффициенте галереи.
    """
    g.glr_conn = sqlite3.connect(__package__.replace('.', '/') + '/galleries.db')


@gallery_bp.teardown_request
def close_db(e):
    """Закрываем подключение к базе данных галерей."""
    g.glr_conn.close()


@gallery_bp.route('/')
def index():
    """Корневая страница блюпринта со списком галерей."""
    c = g.glr_conn.cursor()
    res = c.execute("SELECT title, slug FROM galleries ORDER BY nn;")
    return render_template('gallery/galleries.html', glrs=res, title="Галерея")


def _get_photos(title):
    """По названию галереи возвращает кортежем ее относительный адрес и
    список относительных путей к фотографиям.
    """
    photos_path = __package__.replace('.', '/') + '/static/my_galleries/' + slugify(title)
    _photos = [i for i in os.listdir(photos_path) if i.rpartition('.')[-1].casefold() in
               ('jpg', 'jpeg', 'png', 'apng', 'gif', 'ico', 'svg')]
    photos = [(photos_path + '/' + t).partition('/')[-1] for t in _photos]
    return photos_path, photos


def _get_photos_for_iframe(glr_nn):
    """По номеру галереи glr_nn возвращает список адресов
    фотографий галереи относительно точки входа main.py.
    """
    c = g.glr_conn.cursor()
    res = c.execute("SELECT slug, title FROM galleries WHERE nn = ?;", (glr_nn,)).fetchone()
    if res is None:
        abort(404)

    glr_slug, title = res
    photos_path, photos = _get_photos(title)
    photos = [__package__ + '/' + i for i in photos]
    return photos


@gallery_bp.route('/<glr_slug>')
def glr(glr_slug):
    """Отображение отдельно взятой галереи."""
    c = g.glr_conn.cursor()
    res = c.execute("SELECT nn, title FROM galleries WHERE slug=?;", (glr_slug,)).fetchone()
    if not res:
        abort(404)

    nn, title = res
    photos_path, photos = _get_photos(title)
    thumbnails_path = photos_path + '/thumbnails'
    thumbnails = [os.path.join(thumbnails_path, t).partition('/')[-1] for t in os.listdir(thumbnails_path)]
    thumbnails.sort()

    return render_template('gallery/gallery.html',
                           title=title,
                           thumbnails=thumbnails,
                           nn=str(nn))


@gallery_bp.route('/<int:glr_nn>')
def image(glr_nn):
    """Отображение iframe-а увеличенной фотографии."""
    if not glr_nn:  # убираем мелькание предыдущих фотографий при открытии новой фотографии
        return "<!DOCTYPE html>"

    img = f'{glr_nn}/inner?' + urllib.parse.urlencode(request.args)  # адрес обращения к inner_image
    photos_len = len(_get_photos_for_iframe(glr_nn))

    return render_template('gallery/image.html', img=img, photos_len=photos_len)


@gallery_bp.route('/<int:glr_nn>/inner')
def inner_image(glr_nn):
    """Обработчик возвращающий фотографию в айфрейм из обработчика image."""
    img_n = request.args.get('img_n')

    photos = _get_photos_for_iframe(glr_nn)
    try:  # все, что может вызвать ошибку при неправлильном get-параметре убрал в try
        if int(img_n) <= 0:
            abort(404)
        img = photos[int(img_n) - 1]
    except Exception as e:
        print(e, file=sys.stderr)
        abort(404)

    with open(img, 'rb') as img_f:
        img = img_f.read()

    resp = make_response()
    resp.data = img
    resp.content_type = 'image/jpg'

    return resp
