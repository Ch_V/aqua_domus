"""Микроприложение перенаправляющее HTTP запрос на HTTPS."""

import urllib

from flask import Flask, request, redirect


http_redirect = Flask('http_redirect')


@http_redirect.route('/')
@http_redirect.route('/<path:x>')
def http_to_https(x=None):
    """Для перенаправления HTTP запросов на HTTPS."""
    if urllib.parse.urlparse(request.url).scheme in ('http', ''):
        url = urllib.parse.urlparse(request.url)
        url = url._replace(scheme='https').geturl()
        return redirect(url)
    return "Use HTTPS!"
