"""Блюпринт со статьями."""

import sqlite3

from flask import Blueprint, render_template, g, url_for, abort

from common_components import slugify

articles_bp = Blueprint(name='articles', import_name=__name__, template_folder='templates', static_folder='static')


@articles_bp.before_request
def connect_db():
    """Создаем подключение к базе данных статей с информацией
    об адресе хранения файлов, url-слаге и порядковом коэффициенте статьи.
    """
    g.art_conn = sqlite3.connect(__package__.replace('.', '/') + '/articles.db')


@articles_bp.teardown_request
def close_db(e):
    """Закрываем подключение к базе данных статей."""
    g.art_conn.close()


@articles_bp.route('/')
def index():
    """Корневая страница блюпринта со списком статей."""
    c = g.art_conn.cursor()
    res = c.execute("SELECT title, slug FROM articles ORDER BY nn;")
    return render_template('articles/articles.html', arts=res, title="Статьи")


@articles_bp.route('/<art_slug>')
def art(art_slug):
    """Отображение отдельно взятой статьи."""
    c = g.art_conn.cursor()
    res = c.execute("SELECT title FROM articles WHERE slug=?;", (art_slug,)).fetchone()
    title = None
    if res:
        title = res[0]
    if title is None:
        abort(404)

    html_path = f'articles/my_articles/{art_slug}.html'

    return render_template('articles/article.html', title=title, html_path=html_path)


def add_article_head(html: str, title: str):
    """Добавляет в html-текст статьи заголовок с относительным путем к соответствующим ресурсам в .static"""
    return rf"""<base href="{
            url_for('articles.static', filename='my_articles/' + slugify(title)) + '/'}">""" + '\n' + html
