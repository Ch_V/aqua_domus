"""Модуль с общими компонентами."""

import os
from PIL import Image, UnidentifiedImageError
import re
import sys
import urllib

from flask import request


def slugify(text):
    """Переводим все символы входного текста в url-совместимые."""
    text = text.casefold()
    text = re.sub(r'\s', '_', text)
    text = re.sub(r'[^0-9a-z_а-яё-]', '', text)
    text = text.translate(str.maketrans({
        'а': 'a',
        'б': 'b',
        'в': 'v',
        'г': 'g',
        'д': 'd',
        'е': 'e',
        'ё': 'e',
        'ж': 'j',
        'з': 'z',
        'и': 'i',
        'й': 'i',
        'к': 'k',
        'л': 'l',
        'м': 'm',
        'н': 'n',
        'о': 'o',
        'п': 'p',
        'р': 'r',
        'с': 's',
        'т': 't',
        'у': 'u',
        'ф': 'f',
        'х': 'kh',
        'ц': 'ts',
        'ч': 'ch',
        'ш': 'sh',
        'щ': 'shi',
        'ъ': '',
        'ы': 'i',
        'ь': '',
        'э': 'e',
        'ю': 'yu',
        'я': 'ya',
    }))
    return text


def is_same_host_url(url: str):
    """Проверяет, что ulr-аргумент ведет на тот же хост, что и request.host_url."""
    ref_url = urllib.parse.urlparse(request.host_url)
    test_url = urllib.parse.urlparse(urllib.parse.urljoin(request.host_url, url))
    return re.subn(r'(?:127)\.\d{1,3}\.\d{1,3}\.\d{1,3}:\d{1,4}', 'localhost', ref_url.netloc, 1)[0] ==\
           re.subn(r'(?:127)\.\d{1,3}\.\d{1,3}\.\d{1,3}:\d{1,4}', 'localhost', test_url.netloc, 1)[0]


def generate_thumbnails(path: str, size: int = 250):
    """Создает по указанному адресу субдиректорию 'thumbnails' с миниатюрами изображений из указанной папки.
    Параметр size определяет высоту миниатюры в пикселях.
    """
    # Создаем директорию 'thumbnails', если она еще не создана.
    for filename in os.listdir(path):
        if filename == 'thumbnails':
            if os.path.isdir(os.path.join(path, filename)):
                break
            else:
                raise Exception("WARNING ошибка при чтении изображения в функции 'generate_thumbnails': "
                                "thumbnails не является директорией")
    else:
        os.mkdir(os.path.join(path, 'thumbnails'))

    for img_s in os.listdir(path):
        if os.path.isdir(os.path.join(path, img_s)):
            continue
        with open(os.path.join(path, img_s), 'rb') as img_f:
            try:
                img = Image.open(img_f)
            except UnidentifiedImageError as e:
                print("WARNING ошибка при чтении изображения в функции 'generate_thumbnails': ", e, file=sys.stderr)
            else:
                k = size / img.size[1]  # коэффициент масштабирования
                thumbnail = img.resize([round(k * dim) for dim in img.size])
                thumbnail.save(os.path.join(path, 'thumbnails', img_s))
