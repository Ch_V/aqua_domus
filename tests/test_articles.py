import os
import sqlite3

from flask import url_for
from werkzeug.datastructures import FileStorage

import pytest

from .test_admin import do_login, get_csrf_token
from admin.admin import DeleteArticleForm, ReorderArticleForm
from .conftest import app_

@pytest.mark.parametrize('addr, status', [
    ('articles/voda_v_jizni_rib', 200),
])
def test_first_article(app, client, addr, status):
    """Проверка работоспособности статьи на примере первой статьи."""
    with app.app_context():
        with client:
            res = client.get(addr)
            assert res.status_code == status


@pytest.mark.skipif(not app_.config['ENABLE_ADMINISTRATION'], reason='Administrative function is disabled')
@pytest.mark.usefixtures('keep_articles', 'keep_shadow')
class TestArticles:
    """Тестируем администрирование статей.
    Все тесты из этого класса должны выполняться строго по очереди."""

    def test_delete(self, client):
        """Тестируем удаление статей."""
        with client, sqlite3.connect('file:articles/articles.db?mode=ro', uri=True) as conn:
            cur = conn.cursor()
            assert cur.execute("""SELECT count(title) FROM articles;""").fetchone()[0] == 4  # в БД было 4 статьи
            assert cur.execute("""SELECT count(title) FROM articles WHERE title = 'Вода в жизни рыб';"""
                               ).fetchone()[0] == 1  # в БД есть одна статья 'Вода в жизни рыб'

            assert os.listdir('articles/static/my_articles') == \
                   ['jestkost_obshiaya_gh_i_karbonatnaya_kh',
                    'kislotnost_vodi',
                    'temperatura_vodi',
                    'voda_v_jizni_rib']
            assert os.listdir('articles/templates/articles/my_articles') == \
                   ['jestkost_obshiaya_gh_i_karbonatnaya_kh.html',
                    'kislotnost_vodi.html',
                    'temperatura_vodi.html',
                    'voda_v_jizni_rib.html']

            do_login(client)
            url = url_for('admin.change_articles')
            res = client.post(url,
                              data={
                                  'csrf_token': get_csrf_token(client, url),
                                  'art_title_': 'Вода в жизни рыб',
                              })

            assert len(DeleteArticleForm.choices) == 5 and len(ReorderArticleForm.numbers) == 5
            assert all(i == j for i, j in zip(DeleteArticleForm.choices,
                                              ['',
                                               'Вода в жизни рыб',
                                               'Температура воды',
                                               'Кислотность воды',
                                               'Жесткость общая (GH) и карбонатная (KH)']))
            assert all(i == j for i, j in zip(ReorderArticleForm.numbers,
                                              ['', '1', '2', '3', '4']))

            assert res.status_code == 302
            res = client.get(url)
            assert res.status_code == 200

            assert cur.execute("""SELECT count(title) FROM articles;""").fetchone()[
                       0] == 3  # в БД осталась одна галерея
            assert cur.execute("""SELECT count(title) FROM articles WHERE title = 'Вода в жизни рыб';"""
                               ).fetchone()[0] == 0  # в БД больше нет галереи 'Вода в жизни рыб'

            assert len(DeleteArticleForm.choices) == 4 and len(ReorderArticleForm.numbers) == 4
            assert all(i == j for i, j in zip(DeleteArticleForm.choices,
                                              ['',
                                               'Температура воды',
                                               'Кислотность воды',
                                               'Жесткость общая (GH) и карбонатная (KH)']))
            assert all(i == j for i, j in zip(ReorderArticleForm.numbers,
                                              ['', '1', '2', '3']))

    def test_add(self, client):
        """Тестируем удаление галерей."""
        with open('tests/Resources/my_articles/voda_v_jizni_rib/ftext-1-436x261.jpg',
                  'rb') as f1, \
                open('tests/Resources/my_articles_templates/voda_v_jizni_rib.html',
                     'rb') as f2:
            files = []
            for s, fn in zip((f1, f2), ('1.jpg', '1.html')):
                files.append(FileStorage(
                    stream=s,
                    filename=fn,
                    content_type='multipart/form-data',
                )
                )

            with client, sqlite3.connect('file:articles/articles.db?mode=ro', uri=True) as conn:
                do_login(client)
                url = url_for('admin.change_articles')
                res = client.post(url,
                                  data={
                                      'csrf_token': get_csrf_token(client, url),
                                      'art_title': 'Вода в жизни рыб2',
                                      'files': files
                                  },
                                  content_type='multipart/form-data')

                cur = conn.cursor()
                assert res.status_code == 302
                res = client.get(url)
                assert res.status_code == 200

                assert cur.execute("""SELECT count(title) FROM articles;""").fetchone()[0] == 4  # в БД теперь 4 статьи
                assert cur.execute("""SELECT count(title) FROM articles WHERE title = 'Вода в жизни рыб2';"""
                            ).fetchone()[0] == 1  # в БД появилась статья 'Вода в жизни рыб2'

                assert os.listdir('articles/static/my_articles') == \
                       ['jestkost_obshiaya_gh_i_karbonatnaya_kh', 'kislotnost_vodi', 'temperatura_vodi', 'voda_v_jizni_rib2']

                assert len(DeleteArticleForm.choices) == 5 and len(ReorderArticleForm.numbers) == 5
                assert all(i == j for i, j in zip(DeleteArticleForm.choices,
                                                  ['',
                                                   'Температура воды',
                                                   'Кислотность воды',
                                                   'Жесткость общая (GH) и карбонатная (KH)',
                                                   'Вода в жизни рыб2']))
                assert all(i == j for i, j in zip(ReorderArticleForm.numbers,
                                                  ['', '1', '2', '3', '4']))

    @pytest.mark.parametrize('p_old, p_new, db_res', [
        (1, 4, [2, 3, 4, 1]),
        (1, 2, [2, 1, 3, 4]),
        (4, 1, [4, 1, 2, 3]),
        (4, 3, [1, 2, 4, 3]),
    ])
    def test_reorder(self, client, p_old, p_new, db_res):
        """Тестируем изменение порядка статей."""
        with client, sqlite3.connect('file:articles/articles.db?mode=ro', uri=True) as conn:
            cur = conn.cursor()
            db_state = cur.execute("""SELECT title, slug FROM articles ORDER BY nn;""").fetchall()

            do_login(client)
            url = url_for('admin.change_articles')
            res = client.post(url,
                              data={
                                  'csrf_token': get_csrf_token(client, url),
                                  'position_old': str(p_old),
                                  'position_new': str(p_new),
                              })
            assert res.status_code == 302
            res = client.get(url)
            assert res.status_code == 200

            db_state_res = []
            for i in db_res:
                db_state_res.append(db_state[i - 1])

            db_state_new = cur.execute("""SELECT title, slug FROM articles ORDER BY nn;""").fetchall()
            assert db_state_new == db_state_res

            res = client.post(url,
                              data={
                                  'csrf_token': get_csrf_token(client, url),
                                  'position_old': str(p_new),
                                  'position_new': str(p_old),
                              })
            assert res.status_code == 302
            res = client.get(url)
            assert res.status_code == 200

            db_state_new = cur.execute("""SELECT title, slug FROM articles ORDER BY nn;""").fetchall()

            assert db_state_new == db_state
