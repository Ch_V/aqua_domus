import os, sys; sys.path.append(os.getcwd())
import shutil

import pytest

from app import app as app_


@pytest.fixture()
def app():
    app_.config.update({"TESTING": False, 'PRESERVE_CONTEXT_ON_EXCEPTION': False})
    return app_


@pytest.fixture()
def client(app):
    return app.test_client()


@pytest.fixture()
def keep_shadow(tmpdir):
    """Сохраняет исходный файл shadow, подменяя его на время тестирования."""
    tmp_file = tmpdir.join('shadow')
    shutil.move('admin/shadow', tmp_file)
    shutil.copyfile('tests/Resources/shadow', 'admin/shadow')
    yield
    shutil.move(tmp_file, 'admin/shadow')


def get_prices_filename(startswith: str) -> str:
    """Возвращает имя первого файла в катлоге 'price-lists', чье имя начинается на startswith"""
    for p in os.listdir('price-lists'):
        if p.startswith(startswith):
            filename = p; break
    else:
        print('Исходный файл не найден!!!')
        return None
    return filename


@pytest.fixture()
def keep_stuff(tmpdir):
    """Сохраняет исходный файл Price_AquaDomus_Stuff, подменяя его на время тестирования."""
    filename = get_prices_filename('Price_AquaDomus_Stuff')
    tmp_file = tmpdir.join(filename)
    shutil.move('price-lists/' + filename, tmp_file)
    shutil.copyfile('tests/Resources/Price_AquaDomus_Stuff.xls', 'price-lists/Price_AquaDomus_Stuff.xls')
    yield
    os.remove('price-lists/Price_AquaDomus_Stuff.test')
    shutil.move(tmp_file, 'price-lists/' + filename)


@pytest.fixture()
def keep_services(tmpdir):
    """Сохраняет исходный файл Price_AquaDomus_Services, подменяя его на время тестирования."""
    filename = get_prices_filename('Price_AquaDomus_Services')
    tmp_file = tmpdir.join(filename)
    shutil.move('price-lists/' + filename, tmp_file)
    shutil.copyfile('tests/Resources/Price_AquaDomus_Services.docx', 'price-lists/Price_AquaDomus_Services.docx')
    yield
    os.remove('price-lists/Price_AquaDomus_Services.test')
    shutil.move(tmp_file, 'price-lists/' + filename)


@pytest.fixture(scope='class')
def keep_galleries(tmpdir_factory):
    """Сохраняет базу данных и папки с фотографиями имеющихся галерей, подменяя их на время тестирования."""
    tmp_dir = tmpdir_factory.mktemp('tmp_dir')

    shutil.move('gallery/galleries.db', tmp_dir.join('galleries.db'))
    shutil.copytree('gallery/static/my_galleries', tmp_dir.join('my_galleries'))
    shutil.rmtree('gallery/static/my_galleries')

    shutil.copy('tests/Resources/galleries.db', 'gallery/galleries.db')
    shutil.copytree('tests/Resources/my_galleries',
                    'gallery/static/my_galleries')

    yield

    os.remove('gallery/galleries.db')
    shutil.rmtree('gallery/static/my_galleries')

    shutil.move(tmp_dir.join('galleries.db'), 'gallery/galleries.db')
    shutil.copytree(tmp_dir.join('my_galleries'), 'gallery/static/my_galleries')


@pytest.fixture(scope='class')
def keep_articles(tmpdir_factory):
    """Сохраняет базу данных и папки с фотографиями имеющихся галерей, подменяя их на время тестирования."""
    tmp_dir = tmpdir_factory.mktemp('tmp_dir')

    shutil.move('articles/articles.db', tmp_dir.join('articles.db'))
    shutil.copytree('articles/static/my_articles', tmp_dir.join('my_articles'))
    shutil.rmtree('articles/static/my_articles')
    shutil.copytree('articles/templates/articles/my_articles', tmp_dir.join('my_articles_templates'))
    shutil.rmtree('articles/templates/articles/my_articles')

    shutil.copy('tests/Resources/articles.db', 'articles/articles.db')
    shutil.copytree('tests/Resources/my_articles',
                    'articles/static/my_articles')
    shutil.copytree('tests/Resources/my_articles_templates',
                    'articles/templates/articles/my_articles')

    yield

    os.remove('articles/articles.db')
    shutil.rmtree('articles/static/my_articles')
    shutil.rmtree('articles/templates/articles/my_articles')

    shutil.move(tmp_dir.join('articles.db'), 'articles/articles.db')
    shutil.copytree(tmp_dir.join('my_articles'), 'articles/static/my_articles')
    shutil.copytree(tmp_dir.join('my_articles_templates'), 'articles/templates/articles/my_articles')
