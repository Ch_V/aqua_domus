import os
import sqlite3

from flask import url_for
import pytest
from werkzeug.datastructures import FileStorage

from .test_admin import do_login, get_csrf_token
from admin.admin import DeleteGalleryForm, ReorderGalleryForm
from .conftest import app_


@pytest.mark.parametrize('addr, status', [
    ('gallery/oformlyaem_rastitelnii_akvarium', 200),
    ('gallery/static/my_galleries/oformlyaem_rastitelnii_akvarium/thumbnails/img-20190116-124951-2000x1520.jpg', 200),
    ('gallery/1?img_n=1', 200),
    ('gallery/1/inner?img_n=1', 200),
    ('gallery/0', 200),
    ('gallery/-1', 404),
    ('gallery/1?img_n=-1', 200),
    ('gallery/1?img_n=asdf', 200),
])
def test_first_gallery(app, client, addr, status):
    """Проверка работоспособности галереи на примере первой галереи."""
    with app.app_context():
        with client:
            res = client.get(addr)
            assert res.status_code == status


@pytest.mark.skipif(not app_.config['ENABLE_ADMINISTRATION'], reason='Administrative function is disabled')
@pytest.mark.usefixtures('keep_galleries', 'keep_shadow')
class TestGalleries:
    """Тестируем администрирование галерей.
    Все тесты из этого класса должны выполняться строго по очереди."""

    def test_delete(self, client):
        """Тестируем удаление галерей."""
        with client, sqlite3.connect('file:gallery/galleries.db?mode=ro', uri=True) as conn:
            cur = conn.cursor()
            assert cur.execute("""SELECT count(title) FROM galleries;""").fetchone()[0] == 4  # в БД было 4 галереи
            assert cur.execute("""SELECT count(title) FROM galleries WHERE title = 'Оформляем растительный аквариум';"""
                               ).fetchone()[0] == 1  # в БД есть галерея 'Оформляем растительный аквариум'

            assert os.listdir('gallery/static/my_galleries') == ['asdfg',
                                                                 'oformlyaem_rastitelnii_akvarium',
                                                                 'pereoformyalem_morskoi_ribnik',
                                                                 'qwerty']

            do_login(client)
            url = url_for('admin.change_galleries')
            res = client.post(url,
                              data={
                                  'csrf_token': get_csrf_token(client, url),
                                  'glr_title_': 'Оформляем растительный аквариум',
                              })

            assert len(DeleteGalleryForm.choices) == 5 and len(ReorderGalleryForm.numbers) == 5
            assert all(i == j for i, j in zip(DeleteGalleryForm.choices,
                                              ['',
                                               'Оформляем растительный аквариум',
                                               'Переоформялем морской рыбник',
                                               'qwerty',
                                               'asdfg']))
            assert all(i == j for i, j in zip(ReorderGalleryForm.numbers,
                                              ['', '1', '2', '3', '4']))

            assert res.status_code == 302
            res = client.get(url)
            assert res.status_code == 200

            assert cur.execute("""SELECT count(title) FROM galleries;""").fetchone()[
                       0] == 3  # в БД осталась одна галерея
            assert cur.execute("""SELECT count(title) FROM galleries WHERE title = 'Оформляем растительный аквариум';"""
                               ).fetchone()[0] == 0  # в БД больше нет галереи 'Оформляем растительный аквариум'

            assert os.listdir('gallery/static/my_galleries') == ['asdfg', 'pereoformyalem_morskoi_ribnik', 'qwerty']

            assert len(DeleteGalleryForm.choices) == 4 and len(ReorderGalleryForm.numbers) == 4
            assert all(i == j for i, j in zip(DeleteGalleryForm.choices,
                                              ['', 'Переоформялем морской рыбник', 'qwerty', 'asdfg']))
            assert all(i == j for i, j in zip(ReorderGalleryForm.numbers,
                                              ['', '1', '2', '3']))

    def test_add(self, client):
        """Тестируем добавление галерей."""
        with open('tests/Resources/my_galleries/oformlyaem_rastitelnii_akvarium/img-20190116-124951-2000x1520.jpg',
                  'rb') as f1, \
                open('tests/Resources/my_galleries/oformlyaem_rastitelnii_akvarium/img-20190116-155318-2000x1303.jpg',
                     'rb') as f2:
            files = []
            for s, fn in zip((f1, f2), ('img1.jpg', 'img2.jpg')):
                files.append(FileStorage(
                                        stream=s,
                                        filename=fn,
                                        content_type='multipart/form-data',
                                        )
                             )

            with client, sqlite3.connect('file:gallery/galleries.db?mode=ro', uri=True) as conn:
                do_login(client)
                url = url_for('admin.change_galleries')
                res = client.post(url,
                                  data={
                                      'csrf_token': get_csrf_token(client, url),
                                      'glr_title': 'Оформляем растительный аквариум2',
                                      'files': files
                                        },
                                  content_type='multipart/form-data')

                cur = conn.cursor()
                assert res.status_code == 302
                res = client.get(url)
                assert res.status_code == 200

                assert cur.execute("""SELECT count(title) FROM galleries;""").fetchone()[0] == 4  # в БД теперь 4 галереи
                assert cur.execute("""SELECT count(title) FROM galleries WHERE title = 'Оформляем растительный аквариум2';"""
                                   ).fetchone()[0] == 1  # в БД появилась галерея 'Оформляем растительный аквариум2'

                assert os.listdir('gallery/static/my_galleries') == \
                       ['asdfg', 'oformlyaem_rastitelnii_akvarium2', 'pereoformyalem_morskoi_ribnik', 'qwerty']

                assert len(DeleteGalleryForm.choices) == 5 and len(ReorderGalleryForm.numbers) == 5
                assert all(i == j for i, j in zip(DeleteGalleryForm.choices,
                                                  ['',
                                                   'Переоформялем морской рыбник',
                                                   'qwerty',
                                                   'asdfg',
                                                   'Оформляем растительный аквариум2']))
                assert all(i == j for i, j in zip(ReorderGalleryForm.numbers,
                                                  ['', '1', '2', '3', '4']))

    @pytest.mark.parametrize('p_old, p_new, db_res', [
        (1, 4, [2, 3, 4, 1]),
        (1, 2, [2, 1, 3, 4]),
        (4, 1, [4, 1, 2, 3]),
        (4, 3, [1, 2, 4, 3]),
    ])
    def test_reorder(self, client, p_old, p_new, db_res):
        """Тестируем изменение порядка галерей."""
        with client, sqlite3.connect('file:gallery/galleries.db?mode=ro', uri=True) as conn:
            cur = conn.cursor()
            db_state = cur.execute("""SELECT title, slug FROM galleries ORDER BY nn;""").fetchall()

            do_login(client)
            url = url_for('admin.change_galleries')
            res = client.post(url,
                              data={
                                  'csrf_token': get_csrf_token(client, url),
                                  'position_old': str(p_old),
                                  'position_new': str(p_new),
                              })
            assert res.status_code == 302
            res = client.get(url)
            assert res.status_code == 200

            db_state_res = []
            for i in db_res:
                db_state_res.append(db_state[i - 1])

            db_state_new = cur.execute("""SELECT title, slug FROM galleries ORDER BY nn;""").fetchall()
            assert db_state_new == db_state_res

            res = client.post(url,
                              data={
                                  'csrf_token': get_csrf_token(client, url),
                                  'position_old': str(p_new),
                                  'position_new': str(p_old),
                              })
            assert res.status_code == 302
            res = client.get(url)
            assert res.status_code == 200

            db_state_new = cur.execute("""SELECT title, slug FROM galleries ORDER BY nn;""").fetchall()

            assert db_state_new == db_state

