import hashlib
import io
import os
import re
import shutil

from flask import url_for, session, request
import pytest
from werkzeug.datastructures import FileStorage

from .conftest import get_prices_filename, app_


def get_csrf_token(client, url: str) -> str:
    """Делает GET-запрос по url, получает и парсит форму с csrf-токеном, возвращая его."""
    res_get = client.get(url)
    return re.search(r'(?<=<input id="csrf_token" name="csrf_token" type="hidden" value=").+?(?=">)',
                     res_get.get_data(as_text=True)).group(0)


def do_login(client, pswd='123'):
    """Залогинивает клиента clirnt с паролем pswd."""
    return client.post('/admin/login', data={'csrf_token': get_csrf_token(client, '/admin/login'), 'pswd': pswd})


@pytest.mark.skipif(app_.config['ENABLE_ADMINISTRATION'], reason='Administrative function enabled')
def test_no_logout_on_ENABLE_ADMINISTRATION_False(app, client, keep_shadow):
    """Проверка недоступности странцицы залогинивания при ENABLE_ADMINISTRATION=False."""
    with client:
        res = client.get('/admin/login')
        assert res.status_code == 404


@pytest.mark.skipif(not app_.config['ENABLE_ADMINISTRATION'], reason='Administrative function is disabled')
def test_login_logout(app, client, keep_shadow):
    """Проверка залогинивания правильным паролем."""
    with client:
        res = do_login(client)
        assert res.status_code == 302
        assert session['admin']
        res = client.get('/admin/logout')
        assert res.status_code == 302
        assert not session['admin']


@pytest.mark.skipif(not app_.config['ENABLE_ADMINISTRATION'], reason='Administrative function is disabled')
def test_login_wrong(app, client):
    """Проверка залогинивания неверным паролем."""
    with client:
        do_login(client, pswd='dgdgfgds')
        assert not session.get('admin')


@pytest.mark.skipif(not app_.config['ENABLE_ADMINISTRATION'], reason='Administrative function is disabled')
@pytest.mark.parametrize('url, code_a, code', [
    ('admin.login', 302, 200),
    ('admin.index', 200, 302),
    ('admin.change_pswd', 200, 302),
    ('admin.change_stuff_prices', 200, 302),
    ('admin.change_services_prices', 200, 302),
    ('admin.change_articles', 200, 302),
    ('admin.change_galleries', 200, 302),
    ('admin.backup', 200, 302),
])
def test_admin_urls(client, keep_shadow, url, code_a, code):
    """Тестируем переход на страницы только для админимстратора."""
    with client:
        do_login(client)
        assert client.get(url_for(url)).status_code == code_a

        client.get(url_for('admin.logout'))
        assert client.get(url_for(url)).status_code == code


@pytest.mark.skipif(not app_.config['ENABLE_ADMINISTRATION'], reason='Administrative function is disabled')
@pytest.mark.parametrize('old, new1, new2, is_old_ok, is_new_ok', [
    ('123', 'qwe', 'qwe', True, True),
    ('qwerty', '234', '234', False, True),
    ('123', '234654', '234', True, False),
    ('123dfs', '234654', '234', False, False),
])
def test_change_pswd(app, client, keep_shadow, old, new1, new2, is_old_ok, is_new_ok):
    """Проверяем работу формы смены пароля."""
    with client:
        do_login(client)
        res = client.post(url_for('admin.change_pswd'),
                    data={
                        'csrf_token': get_csrf_token(client, url_for('admin.change_pswd')),
                        'old_pswd': old,
                        'new_pswd': new1,
                        'new_pswd_bis': new2,
                    })

        # Проверяем вывод flash сообщений
        if not is_old_ok:
            assert 'Неверный старый пароль' in res.get_data(as_text=True)
        else:
            assert 'Неверный старый пароль' not in res.get_data(as_text=True)
        if not is_new_ok:
            assert 'Новый пароль введен не одинаково' in res.get_data(as_text=True)
        else:
            assert 'Новый пароль введен не одинаково' not in res.get_data(as_text=True)
        if is_old_ok and is_new_ok:
            assert 'Пароль успешно изменен' in res.get_data(as_text=True)
        else:
            assert 'Пароль успешно изменен' not in res.get_data(as_text=True)

        client.get(url_for('admin.logout'))
        assert not session.get('admin')

        if is_old_ok and is_new_ok:
            pswd_ok = new1
            pswd_wrong = old
        else:
            pswd_ok = '123'
            pswd_wrong = new1

        do_login(client, pswd=pswd_wrong)
        assert not session.get('admin')
        do_login(client, pswd=pswd_ok)
        assert session['admin']


@pytest.mark.skipif(not app_.config['ENABLE_ADMINISTRATION'], reason='Administrative function is disabled')
def test_backup(app, client, keep_shadow):
    """Проверка резервного копирования."""
    filename = 'aquadomus.zip'

    try:
        os.remove(filename)
    except FileNotFoundError:
        pass

    do_login(client)

    with app.test_request_context():
        res = client.get(url_for('admin.backup'))

    assert res.status_code == 200
    assert os.path.isfile(filename)
    assert os.path.getsize(filename) > 1000


def prices(app, client, startswith, url, worker):
    """Функция, используемая в послудующих двух тестах (смотри их)"""
    def get_file_hash():
        """Считаем MD-5 хеш файла цен на товары, используемого в данный момент."""
        filename = get_prices_filename(startswith)
        with open('price-lists/' + filename, 'rb') as f:
            hash = hashlib.md5(f.read()).hexdigest()
        return hash

    old_hash = get_file_hash()

    mock_file = FileStorage(
        stream=io.BytesIO(b'abcdef'),
        filename='MyTestFilename.test',
        content_type='multipart/form-data',
    )
    with client:
        do_login(client)
        res = client.post(url,
                          data={
                              'csrf_token': get_csrf_token(client, url),
                              'file': mock_file,
                          })

    new_hash = get_file_hash()

    assert new_hash != old_hash
    assert new_hash.lower() == hashlib.md5(b'abcdef').hexdigest()  # проверяем, что это именно новый файл

    # Проверяем, что файл по-прежнему грузится
    with app.test_request_context():
        assert client.get(url_for(worker)).status_code == 200


@pytest.mark.skipif(not app_.config['ENABLE_ADMINISTRATION'], reason='Administrative function is disabled')
def test_change_stuff_prices(client, app, keep_shadow, keep_stuff):
    """Проверяем функцию замены файла цен на товары."""
    with app.test_request_context():
        prices(app, client, 'Price_AquaDomus_Stuff', url_for('admin.change_stuff_prices'), 'stuff')


@pytest.mark.skipif(not app_.config['ENABLE_ADMINISTRATION'], reason='Administrative function is disabled')
def test_change_services_prices(client, app, keep_shadow, keep_services):
    """Проверяем функцию замены файла цен на товары."""
    with app.test_request_context():
        prices(app, client, 'Price_AquaDomus_Services', url_for('admin.change_services_prices'), 'services')
