from flask import url_for
import pytest
from werkzeug.routing import BuildError

from .conftest import app_


@pytest.mark.parametrize('url, code', [
    ('index', 200),
    ('stuff', 200),
    ('services', 200),
    ('contacts', 200),
    ('articles.index', 200),
    ('gallery.index', 200),
    ('login', 301),
    ('admin.login', 200),
    ('admin.index', 302),
    ('admin.change_pswd', 302),
    ('admin.change_stuff_prices', 302),
    ('admin.change_services_prices', 302),
    ('admin.change_articles', 302),
    ('admin.change_galleries', 302),
    ('admin.backup', 302),
])
def test_base_urls(app, client, url, code):
    """Тестируем переход на основные страницы."""
    with app.test_request_context():
        if app_.config['ENABLE_ADMINISTRATION'] or not url.startswith('admin'):
            assert client.get(url_for(url)).status_code == code
        else:
            with pytest.raises(BuildError):
                url_for(url)


@pytest.mark.parametrize('url', [
    '/asdgfdfgdfg',
    '/articles/fersqe',
    '/gallery/fersqe',
])
def test_404(client, url):
    """Тестируем переход по неверному адресу."""
    assert client.get(url).status_code == 404
