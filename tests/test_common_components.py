import re
import os
from PIL import Image, UnidentifiedImageError
import shutil

import pytest

from common_components import slugify, is_same_host_url, generate_thumbnails


@pytest.mark.parametrize('text',
    (
    r'Привет, мир!-12?:',
    r'gh gkjлд\роф@# >> ?gHj/f& ^* ываРЛоргUIKYh -=3',
    ), ids=range(2))
def test_slugify(text):
    """Проверяем корректность перевода любого текста в url-совместимый."""
    text = slugify(text)
    text = re.sub(r'[0-9a-z_-]', '', text)
    assert len(text) == 0


@pytest.mark.parametrize('url, res', [
    ('http://127.0.0.1:5000/', True),
    ('/qwe', True),
    ('http://other_url.org/', False),
    ('https://other_url.net/qwe?asd=543', False),
])
def test_is_same_host_url(app, url, res):
    """Проверяем функцию определения того, что введенный url ссылается на тот-же домен, что и само приложение.
    Тестовые примеры написаны для приложения запускаемого с локалхоста.
    """
    with app.test_request_context():
        assert is_same_host_url(url) == res


@pytest.mark.parametrize('thumbnail_already_exists, size', [(False, 180), (True, 300)])
def test_generate_thumbnails(tmpdir, capsys, thumbnail_already_exists, size):
    """Тестируем функцию generate_thumbnails."""
    for filename in ('img-20190116-124951-2000x1520.jpg',
                     'img-20190118-221010-2000x4000.jpg',
                     'Price_AquaDomus_Stuff.xls'): # некартинку тоже добавим
        shutil.copy(os.path.join('tests/Resources', filename), os.path.join(tmpdir, filename))
    os.makedirs(os.path.join(tmpdir, 'directory'))
    if thumbnail_already_exists:
        os.makedirs(os.path.join(tmpdir, 'thumbnails'))

    generate_thumbnails(tmpdir, size)

    assert capsys.readouterr().err.startswith('WARNING ошибка при чтении изображения generate_thumbnails')

    img_files = []
    for filename in os.listdir(os.path.join(tmpdir, 'thumbnails')):
        if os.path.isfile(os.path.join(tmpdir, 'thumbnails', filename)):
            with open(os.path.join(tmpdir, 'thumbnails', filename), 'rb') as file:
                try:
                    img = Image.open(file)
                except UnidentifiedImageError:
                    pass
                else:
                    img_files.append(img)
                    assert img.size[1] == size

    assert len(img_files) == 2  # проверяемБ что были созданы именно 2 миниатюры
