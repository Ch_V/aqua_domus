"""Модуль в котором создаем и конфигурируем приложение."""

import os
import urllib
import re

from flask import Flask, request, redirect
from flask_paranoid import Paranoid

import constants
from articles import articles
from gallery import gallery
from admin import admin

app = Flask(__name__)
app.config.from_object(constants)
app.config['BACKGROUND_IMAGES'] = os.listdir('static/bg_images')

app.register_blueprint(articles.articles_bp, url_prefix='/articles')
app.register_blueprint(gallery.gallery_bp, url_prefix='/gallery')
if app.config['ENABLE_ADMINISTRATION']:
    app.register_blueprint(admin.admin_bp, url_prefix='/admin')

# from werkzeug.middleware.profiler import ProfilerMiddleware
# app.wsgi_app = ProfilerMiddleware(app.wsgi_app, sort_by=('time',), restrictions=(10,))

import views  # Регистрируем определенные в модуле views обработчики


@app.before_request
def before_request():
    """Перенаправляем нелокальные запросы на HTTPS, если установлен флаг ENFORCE_HTTPS."""
    netloc = urllib.parse.urlparse(request.url).netloc
    if (app.config['ENFORCE_HTTPS']
        and not request.is_secure
        and netloc != 'localhost'
        and not re.match(r'127\.\d{1,3}\.\d{1,3}\.\d{1,3}(:.*)?', netloc)):

        return redirect(request.url.replace('http://', 'https://', 1), code=301)


paranoid = Paranoid(app)
paranoid.redirect_view = '/'
