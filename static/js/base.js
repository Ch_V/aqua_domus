"use strict";


document.getElementById('JS_WARNING').style.display = 'none';


// Прямоугольник поля меню 'Цены' и всего верхнего колонтитула.
let rect_p, rect_c;

// Целевой всплывающий элемент с прайс-листами.
let target = document.getElementById('div_prices');
let rect_t;  // Прямоугольник целевого всплывающего элемента с прайс-листами.


function place_prices() {
    /* Размещаем прайс листы в нужном месте. */
    rect_p = document.getElementById('prices').getBoundingClientRect();
    rect_c = document.getElementById('colontitule').getBoundingClientRect();
    target.style.left = '0';
    rect_t = target.getBoundingClientRect();
    target.style.top = String(Number(rect_c.bottom) + 5) + 'px';
    target.style.left = String(
        Number(rect_p.left) + Number(rect_p.width) / 2  //  середина прямоугольника 'Цена' по x
        - Number(rect_t.width) / 2
    ) + 'px';
}


window.onresize = function(e) {
    if (target.style.visibility == 'visible') place_prices();
}


function show_prices() {
    /* Отображаем прайс-листы при клике мышкой по 'Цена'. */
    if (target.style.visibility == 'visible') {
        target.style.visibility = 'hidden';
    } else {
        target.style.visibility = 'visible';
        place_prices();
    }
}


document.addEventListener('mouseup', function(e) {
    /* Скрываем прайс-листы при клике мышкой мимо них. */
    if ((!target.contains(e.target)) &&
        (!document.getElementById('prices').contains(e.target))) {
        target.style.visibility = 'hidden';
    }
});


document.onscroll = function(e) {
    /* Скрываем прайс-листы при прокручивании */
    if (target.style.visibility == 'visible') target.style.visibility = 'hidden';
}
