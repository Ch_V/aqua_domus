"""Точка запуска приложения c werkzeug-сервером. DEVELOPMENT ONLY!"""
from app import app


app.run(host=app.config['HOST'],
        port=app.config['PORT'],
        debug=app.config['DEBUG'],
        ssl_context=('TEST_ONLY_server.crt', 'TEST_ONLY_server.key') if app.config['ENFORCE_HTTPS'] else None)
