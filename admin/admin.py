"""Блюпринт администрирования."""

from contextlib import contextmanager
from functools import wraps
import glob
import os
import re
import shutil
import sqlite3
import sys
from zipfile import ZipFile

from flask import Blueprint, render_template, url_for, request, session, flash, redirect, send_file, current_app
from werkzeug.security import generate_password_hash, check_password_hash
from werkzeug.utils import secure_filename
from flask_wtf import FlaskForm
from wtforms import validators, StringField, PasswordField, FileField, MultipleFileField, SelectField
from common_components import is_same_host_url

from articles.articles import add_article_head
from common_components import slugify, generate_thumbnails
from constants import PASSWORD_HASHING_ITERATIONS


admin_bp = Blueprint(name='admin', import_name=__name__, template_folder='templates', static_folder='static')


def admin_only(f):
    """Декоратор - аналог @login_required из Flask-Login.
    !!! Необходимо применять НИЖНИМ декоратором (до @...route()) !!!
    """
    @wraps(f)
    def wrapper(*args, **kwargs):
        if session.get('admin'):
            return f(*args, **kwargs)

        return redirect(url_for('admin.login', came_from=request.url))

    return wrapper


@contextmanager
def in_process():
    """Контекстный менеджер установки-снятия флаге IN_PROCESS."""
    current_app.config['IN_PROCESS'] = True
    yield
    current_app.config['IN_PROCESS'] = False


def check_in_process(endpoint):
    """Функция прерывания при установленном флаге IN_PROCESS."""
    if current_app.config.get('IN_PROCESS', False):
        flash('Дождитесь окончания уже запущенных процессов администрирования.', category='fail')
        return redirect(url_for(endpoint))


@admin_bp.route('/')
@admin_only
def index():
    """Главная страница администрирования."""
    return render_template('admin/admin.html', title='Admin')


class LoginForm(FlaskForm):
    """Форма входа админимтратора."""
    pswd = PasswordField('Пароль: ', name='pswd', validators=[validators.DataRequired()])


@admin_bp.route('/login', methods=['GET', 'POST'])
def login():
    """Страница залогинивания и валидации введенного пароля."""
    if session.get('admin'):
        return redirect(url_for('.index'))
    form = LoginForm()
    if request.method == 'POST' and form.validate_on_submit():
        with open(url_for('.static', filename='')[1:-1].rpartition('/')[0] + '/shadow') as shadow:
            shadow_str = shadow.readline().strip()
            if check_password_hash(shadow_str, form.pswd.data):
                session['admin'] = True
                came_from = request.args.get('came_from')
                if not came_from:
                    return redirect(url_for('.index'))
                elif is_same_host_url(came_from):
                    return redirect(came_from)
                else:
                    print('WARNING - Перенаправление на непренадлежащий хосту URL!', file=sys.stderr)
                    return redirect(url_for('.index'), 302)
            else:
                flash('Неверный пароль!  :-(', category='fail')

    return render_template('admin/login.html', form=form, title='Login')


@admin_bp.route('/logout')
def logout():
    """Разлогинивание."""
    session['admin'] = False
    if request.referrer:
        if 'admin' not in request.referrer:
            return redirect(request.referrer)

    return redirect(url_for('index'))


class ChangePswdForm(FlaskForm):
    """Форма изменения пароля."""
    old_pswd = PasswordField('Старый пароль: ', validators=[validators.DataRequired()])
    new_pswd = PasswordField('Новый пароль: ', validators=[validators.DataRequired()])
    new_pswd_bis = PasswordField('Новый пароль еще раз: ', validators=[validators.DataRequired()])


@admin_bp.route('/change_pswd', methods=['GET', 'POST'])
@admin_only
def change_pswd():
    """Страница-обработчик смены пароля."""

    form = ChangePswdForm()

    if request.method == 'POST' and form.validate_on_submit():

        with in_process():
            with open(url_for('.static', filename='')[1:-1].rpartition('/')[0] + '/shadow', 'r+') as shadow:
                shadow_str = shadow.readline().strip()
                if check_password_hash(shadow_str, form.old_pswd.data) and \
                        form.new_pswd.data == form.new_pswd_bis.data:
                    shadow.seek(0)
                    shadow.write(generate_password_hash(form.new_pswd.data,
                                                        method=f'pbkdf2:sha256:{PASSWORD_HASHING_ITERATIONS}'))
                    shadow.truncate()
                    flash('Пароль успешно изменен :-)', category='success')
                else:
                    if not check_password_hash(shadow_str, form.old_pswd.data):
                        flash('Неверный старый пароль :-(', category='fail')
                    if form.new_pswd.data != form.new_pswd_bis.data:
                        flash('Новый пароль введен не одинаково :-(', category='fail')

    return render_template('admin/change_pswd.html', form=form, title='Изменение пароля')


@admin_bp.route('/backup')
@admin_only
def backup():
    """Производит резервное копирование файлов приложения предлагая для скачивания архив с файлами."""
    ret = check_in_process('.index')
    if ret: return ret

    with in_process():
        filename = './aquadomus.zip'
        with ZipFile(filename, 'w') as zfile:
            for i in glob.iglob('**', recursive=True):
                if not i.startswith('.') \
                        and '__pycache__' not in os.path.normpath(i).split(os.sep) \
                        and not i.endswith(('.tar', '.gz', '.zip')):
                    zfile.write(i)

    return send_file(filename, as_attachment=True)


class ChangePricesForm(FlaskForm):
    file = FileField(validators=[validators.DataRequired()])


def _change_prices(file_name: 'str, без расширения', price_on: 'str, на что цены'):
    """Используется в обработчиках ниже ниже для замены файла с ценами."""
    form = ChangePricesForm()
    if request.method == 'POST' and form.validate_on_submit() and 'file' in request.files:
        with in_process():
            file = request.files['file']
            if file.filename != '':
                # удаляем старый файл
                for p in os.listdir('price-lists'):
                    if p.startswith(file_name):
                        os.remove('price-lists/' + p)
                # добавляем новый файл
                name = list(secure_filename(file.filename).rpartition('.'))
                name[0] = file_name
                name = ''.join(name)
                file.save('price-lists/' + name)
                flash(f'Файл с ценами на {price_on} успешно заменен', category='success')

    elif request.method == 'POST':
        flash('Что-то пошло не так :-(', category='fail')

    return render_template('admin/change_prices.html',
                           form=form,
                           header=f'Замена файла с ценами на {price_on}',
                           title=f'Замена файла с ценами на {price_on}',
                           )


@admin_bp.route('/change_stuff_prices', methods=['GET', 'POST'])
@admin_only
def change_stuff_prices():
    """Страница-обработчик замены файла цен на товары."""
    if request.method == 'POST':
        ret = check_in_process('.change_stuff_prices')
        if ret: return ret
    return _change_prices('Price_AquaDomus_Stuff', 'товары')


@admin_bp.route('/change_services_prices', methods=['GET', 'POST'])
@admin_only
def change_services_prices():
    """Страница-обработчик замены файла цен на услуги."""
    if request.method == 'POST':
        ret = check_in_process('.change_services_prices')
        if ret: return ret
    return _change_prices('Price_AquaDomus_Services', 'услуги', )


class UploadArticleForm(FlaskForm):
    """Форма загрузки статьи."""
    art_title = StringField(validators=[validators.DataRequired()])
    files = MultipleFileField(validators=[validators.DataRequired()])


class DeleteArticleForm(FlaskForm):
    """Форма удаления статьи."""
    choices = []
    art_title_ = SelectField(choices=choices, validators=[validators.DataRequired()])


class ReorderArticleForm(FlaskForm):
    """Форма изменения порядка статей."""
    numbers = []
    position_old = SelectField(choices=numbers, validators=[validators.DataRequired()])
    position_new = SelectField(choices=numbers, validators=[validators.DataRequired()])


@admin_bp.route('/change_articles', methods=['GET', 'POST'])
@admin_only
def change_articles():
    """Страница-обработчик добавления, удаления и переупорядочивания статей."""
    upload_form = UploadArticleForm()

    DeleteArticleForm.choices.clear()
    DeleteArticleForm.choices.append('')

    ReorderArticleForm.numbers.clear()
    ReorderArticleForm.numbers.append('')

    with sqlite3.connect('articles/articles.db') as conn:
        cur = conn.cursor()
        max_nn = cur.execute("""SELECT max(nn) FROM articles;""").fetchone()[0]
        if max_nn is None: max_nn = 0

        DeleteArticleForm.choices_ = (i[0] for i in cur.execute("""SELECT title FROM articles ORDER BY nn;""").fetchall())
        DeleteArticleForm.choices.extend(DeleteArticleForm.choices_)  # составляем выпадающий список названия статей
        delete_form = DeleteArticleForm()

        ReorderArticleForm.numbers.extend((str(i) for i in range(1, max_nn + 1)))
        reorder_form = ReorderArticleForm()

        if request.method == 'POST' and any((upload_form.validate_on_submit(),
                                             delete_form.validate_on_submit(),
                                             reorder_form.validate_on_submit())):
            ret = check_in_process('.change_articles')
            if ret: return ret

            with in_process():

                ### ДОБАВЛЯЕМ СТАТЬЮ
                if upload_form.validate_on_submit():
                    slug = slugify(upload_form.art_title.data)
                    slugs = [i[0] for i in cur.execute("""SELECT slug FROM articles;""").fetchall()]

                    # Проверяем уникальность слага-названия статьи
                    slug_ok = True
                    if slug in slugs:
                        flash('Статья с похожим названием уже существует!', category='fail')
                        slug_ok = False

                    # Проверяем на наличие одного HTML-файла и на отсутствие пробелов в именах
                    html_included, html_unique, names_without_spaces = False, True, True
                    for file in request.files.getlist("files"):
                        if html_included and file.filename.endswith(('.html', '.htm')):
                            html_unique = False
                        if file.filename.endswith(('.html', '.htm')):
                            html_included = True
                        for fn in file.filename:
                            for s in fn:
                                if not s.isascii():
                                    names_without_spaces = False
                    if not html_included: flash('Среди загружаемых файлов не найтен HTML-файл', category='fail')
                    if not html_unique: flash('Среди загружаемых файлов должен быть только один HTML-файл', category='fail')
                    if not names_without_spaces: flash('Имена файлов содержат недопустимые символы', category='fail')

                    if all((slug_ok, html_included, html_unique, names_without_spaces)):

                        # В первую очередь добавляем запись о статье в БД.
                        cur.execute("""BEGIN;""")
                        cur.execute("""INSERT INTO articles(nn, title, slug) VALUES (?, ?, ?)""",
                                    (max_nn + 1, upload_form.art_title.data, slug))

                        try:
                            for file in request.files.getlist("files"):
                                filename = secure_filename(file.filename)

                                # Не загружаем файлы указанных расширений
                                for ext in ('.css', '.exe', '.bat', '.sh', '.py', '.pyw', '.pyc'):
                                    if filename.endswith(ext):
                                        continue

                                # Обработка HTML. Оставляем только содержимое BODY
                                if filename.endswith(('.html', '.htm')):
                                    html_text = str(file.stream.read(), encoding='UTF-8')
                                    match = re.search(r'(?<=<body>).*(?=</body>)', html_text, flags=re.S)
                                    html_text = match.group(0) if match else html_text  # если загрузят целиком с head и body
                                    html_text = add_article_head(html_text, upload_form.art_title.data)
                                    with open(f'articles/templates/articles/my_articles/{slug}.html',
                                              'x', errors='ignore', newline="", encoding='utf-8') as f:
                                        f.write(html_text)

                                # Обрабатывам не HTML-файлы
                                else:
                                    dir_url = url_for('articles.static', filename='my_articles/' + slug)[1:]
                                    try:
                                        os.mkdir(dir_url)
                                    except FileExistsError:
                                        pass
                                    file.save(dir_url + '/' + filename)
                        except Exception:
                            conn.rollback()
                        else:
                            conn.commit()
                            flash('Статья добавлена', category='success')

                        return redirect(request.url)

                ### УДАЛЯЕМ СТАТЬЮ
                if delete_form.validate_on_submit():
                    slug, nn = cur.execute("""SELECT slug, nn FROM articles WHERE title = ?""",
                                           (delete_form.art_title_.data,)).fetchone()
                    files_dir = url_for('articles.static', filename='my_articles/' + slug)[1:]
                    shutil.rmtree(files_dir, ignore_errors=True)

                    os.remove(f'articles/templates/articles/my_articles/{slug}.html')

                    cur.execute("""BEGIN;""")
                    cur.execute("""DELETE FROM articles WHERE nn = ?;""", (nn,))
                    cur.execute("""UPDATE articles SET nn = nn - 1 WHERE nn > ?;""", (nn,))
                    conn.commit()

                    flash('Статья удалена успешно', category='success')
                    return redirect(request.url)

                ### МЕНЯЕМ ПОРЯДОК СТАТЕЙ
                if reorder_form.validate_on_submit():
                    if reorder_form.position_old.data > reorder_form.position_new.data:
                        position_old = int(reorder_form.position_old.data) + 1
                        position_new = int(reorder_form.position_new.data)
                    else:
                        position_old = int(reorder_form.position_old.data)
                        position_new = int(reorder_form.position_new.data) + 1

                    cur.execute("""BEGIN;""")
                    cur.execute("""UPDATE articles SET nn = - (nn + 1) WHERE nn >= ?;""", (position_new,))
                    cur.execute("""UPDATE articles SET nn = - nn WHERE nn < 0;""")

                    cur.execute("""UPDATE articles SET nn = ? WHERE nn = ?""", (position_new, position_old))
                    cur.execute("""UPDATE articles SET nn = nn - 1 WHERE nn >= ?;""", (position_old,))
                    conn.commit()

                    flash('Статья перемещена успешно', category='success')
                    return redirect(request.url)

        return render_template('admin/change_articles.html',
                               upload_form=upload_form,
                               delete_form=delete_form,
                               reorder_form=reorder_form,
                               title='Редактирование статей')


class UploadGalleryForm(FlaskForm):
    """Форма загрузки статьи."""
    glr_title = StringField(validators=[validators.DataRequired()])
    files = MultipleFileField(validators=[validators.DataRequired()])


class DeleteGalleryForm(FlaskForm):
    """Форма удаления статьи."""
    choices = []
    glr_title_ = SelectField(choices=choices, validators=[validators.DataRequired()])


class ReorderGalleryForm(FlaskForm):
    """Форма изменения порядка статей."""
    numbers = []
    position_old = SelectField(choices=numbers, validators=[validators.DataRequired()])
    position_new = SelectField(choices=numbers, validators=[validators.DataRequired()])


@admin_bp.route('/change_galleries', methods=['GET', 'POST'])
@admin_only
def change_galleries():
    """Страница-обработчик добавления, удаления и переупорядочивания галерей."""
    upload_form = UploadGalleryForm()

    DeleteGalleryForm.choices.clear()
    DeleteGalleryForm.choices.append('')

    ReorderGalleryForm.numbers.clear()
    ReorderGalleryForm.numbers.append('')

    with sqlite3.connect('gallery/galleries.db') as conn:
        cur = conn.cursor()
        max_nn = cur.execute("""SELECT max(nn) FROM galleries;""").fetchone()[0]
        if max_nn is None: max_nn = 0

        DeleteGalleryForm.choices_ = (i[0] for i in cur.execute("""SELECT title FROM galleries ORDER BY nn;""").fetchall())
        DeleteGalleryForm.choices.extend(DeleteGalleryForm.choices_)  # составляем выпадающий список названия статей
        delete_form = DeleteGalleryForm()

        ReorderGalleryForm.numbers.extend((str(i) for i in range(1, max_nn + 1)))
        reorder_form = ReorderGalleryForm()

        if request.method == 'POST' and any((upload_form.validate_on_submit(),
                                             delete_form.validate_on_submit(),
                                             reorder_form.validate_on_submit())):
            ret = check_in_process('.change_galleries')
            if ret: return ret

            with in_process():

                ### ДОБАВЛЯЕМ ГАЛЕРЕЮ
                if upload_form.validate_on_submit():
                    slug = slugify(upload_form.glr_title.data)
                    slugs = [i[0] for i in cur.execute("""SELECT title, slug FROM galleries;""").fetchall()]

                    success = True

                    # Проверяем уникальность слага-названия галереи
                    if slug in slugs:
                        flash('Галерея с похожим названием уже существует!', category='fail')
                        success = False

                    # Проверяем формат файлов по расширениям
                    for file in request.files.getlist("files"):
                        if file.filename.rpartition('.')[-1] not in ('jpg', 'jpeg', 'png', 'apng', 'gif', 'ico', 'svg'):
                            flash("Среди выбранных файлов имеются файлы недопустимого формата. "
                                  "Допустимы только 'jpg', 'jpeg', 'png', 'apng', 'gif', 'ico', 'svg'",
                                  category='fail')
                            success = False
                            break

                    if success:
                        # В первую очередь добавляем запись о галерее в БД.
                        cur.execute("""BEGIN;""")
                        cur.execute("""INSERT INTO galleries(nn, title, slug) VALUES (?, ?, ?)""",
                                    (max_nn + 1, upload_form.glr_title.data, slug))
                        try:
                            for file in request.files.getlist("files"):
                                filename = secure_filename(file.filename)
                                dir_url = url_for('gallery.static', filename='my_galleries/' + slug)[1:]
                                try:
                                    os.mkdir(dir_url)
                                except FileExistsError:
                                    pass
                                file.save(dir_url + '/' + filename)

                                generate_thumbnails(dir_url)


                        except Exception:
                            conn.rollback()
                        else:
                            conn.commit()
                            flash('Галерея добавлена', category='success')

                        return redirect(request.url)

                ### УДАЛЯЕМ ГАЛЕРЕЮ
                if delete_form.validate_on_submit():
                    slug, nn = cur.execute("""SELECT slug, nn FROM galleries WHERE title = ?""",
                                           (delete_form.glr_title_.data,)).fetchone()
                    files_dir = url_for('gallery.static', filename='my_galleries/' + slug)[1:]
                    shutil.rmtree(files_dir, ignore_errors=True)

                    cur.execute("""BEGIN;""")
                    cur.execute("""DELETE FROM galleries WHERE nn = ?""", (nn,))
                    cur.execute("""UPDATE galleries SET nn = nn - 1 WHERE nn > ?;""", (nn,))
                    conn.commit()

                    flash('Галерея удалена успешно', category='success')
                    return redirect(request.url)

                ### МЕНЯЕМ ПОРЯДОК ГАЛЕРЕЙ
                if reorder_form.validate_on_submit():
                    if reorder_form.position_old.data > reorder_form.position_new.data:
                        position_old = int(reorder_form.position_old.data) + 1
                        position_new = int(reorder_form.position_new.data)
                    else:
                        position_old = int(reorder_form.position_old.data)
                        position_new = int(reorder_form.position_new.data) + 1

                    cur.execute("""BEGIN;""")
                    cur.execute("""UPDATE galleries SET nn = - (nn + 1) WHERE nn >= ?;""", (position_new,))
                    cur.execute("""UPDATE galleries SET nn = - nn WHERE nn < 0;""")

                    cur.execute("""UPDATE galleries SET nn = ? WHERE nn = ?""", (position_new, position_old))
                    cur.execute("""UPDATE galleries SET nn = nn - 1 WHERE nn >= ?;""", (position_old,))
                    conn.commit()

                    flash('Галерея перемещена успешно', category='success')
                    return redirect(request.url)

        return render_template('admin/change_galleries.html',
                               upload_form=upload_form,
                               delete_form=delete_form,
                               reorder_form=reorder_form,
                               title='Редактирование галерей')
